name := """minbakCrawling"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache,
  ws,
  "jp.t2v" %% "stackable-controller" % "0.4.0",
  "com.typesafe.play.plugins" %% "play-plugins-mailer" % "2.3.0"
)
