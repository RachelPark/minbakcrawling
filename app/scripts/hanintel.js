

casper.test.begin('Minbak Crawling - Rachel', 0, function(test){
    casper.start();

    area = casper.cli.get("area");
    count = casper.cli.get("count");

    var hanintel_url = 'http://www.hanintel.com/'
    var hanintel_search_url = hanintel_url+"guesthouse/list?hmp="+count+"&marea="+area;

    casper.thenOpen(hanintel_url, function() {

        this.echo(this.getTitle());
        this.capture('open_page_hanintel.png');
    });


    casper.thenOpen(hanintel_search_url, function(){

        var fs = require('fs');

        this.capture('after_search_page_hanintel.png');

        var hrefs = this.getElementsAttribute('ul.listCard li div.btn_guesthouse_open span a', 'href');
        var stored_hrefs = "";
        for(var i=0; i<hrefs.length; i++){
            var href = hrefs[i];
            this.echo("href[" + i + "]: " + href);
            stored_hrefs = stored_hrefs + href +"\n";
        }
        fs.write('/Users/Rachel/Documents/workspace-scala/minbakCrawling/app/scripts/hanintel_href.txt', stored_hrefs, 'w');

        var imgSrces = this.getElementsAttribute('section#house_list ul.listCard li a img', 'src');
        var stored_imgSrces = "";
        for(var i=0; i<imgSrces.length; i++){
            var imgSrc = imgSrces[i];
            this.echo("img src[" + i + "]: " + imgSrc)
            stored_imgSrces = stored_imgSrces + imgSrc + "\n";
        }
        fs.write('/Users/Rachel/Documents/workspace-scala/minbakCrawling/app/scripts/hanintel_imgSrc.txt', stored_imgSrces, 'w');

    });

    casper.run(function(){
        this.test.done();
    });
});