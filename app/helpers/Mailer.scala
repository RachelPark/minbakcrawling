package helpers

import play.api.Play.current
import com.typesafe.plugin._

import scala.concurrent.duration._



object Mailer {

  def send(recipient: String, subject: String, body: String) {
    send("mjpark03@gmail.com", Some("mjpark03@gmail.com"), recipient, subject, body);
  }

  def send(from: String, bcc: Option[String], recipient: String, subject: String, body: String) {
    val mail = use[MailerPlugin].email
    mail.setFrom(from)
    mail.setSubject(subject)
    mail.setRecipient(recipient)
    if (!bcc.getOrElse("").isEmpty) {
      mail.setBcc(bcc.getOrElse(""))
    }

    mail.sendHtml(body)
  }
}

