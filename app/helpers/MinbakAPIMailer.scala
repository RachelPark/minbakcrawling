package helpers

import scala.math.BigDecimal



object MinbakAPIMailer {
  val korbit_link = play.api.Play.current.configuration.getString("url.korbit").getOrElse("https://www.korbit.co.kr");

  def getEmailSearchResultBody(email: String, hrefs: List[String], imgSrcs: List[String]) = {

    val starting_message = s"Hi, This is the result for your requests"
    val content= s"This is the result for your requests"

    views.html.resultEmailView(starting_message, content, hrefs, imgSrcs).toString()
  }

}