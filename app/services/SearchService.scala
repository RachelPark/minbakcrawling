package services

import helpers.{MinbakAPIMailer, Config, Mailer}

import scala.sys.process._
import scala.io._
import scala.io.Source._

import sys.process.stringSeqToProcess



object SearchService {

  def requestSearch(area: String, count: Int) = {

    val code = findCodeForHanintel(area)
    executeScript(code, count)

    val hrefsFile = "/Users/Rachel/Documents/workspace-scala/minbakCrawling/app/scripts/hanintel_href.txt"
    val imgSrcesFile = "/Users/Rachel/Documents/workspace-scala/minbakCrawling/app/scripts/hanintel_imgSrc.txt"
    var hrefsList = List[String]();
    var imgSrcesList = List[String]();

    for (line <- Source.fromFile(hrefsFile).getLines()) {
      hrefsList ::= (Config.hanintelUrl + line)
      println(s"[info] result: ${hrefsList}")
    }

    for (line <- Source.fromFile(imgSrcesFile).getLines()) {
      imgSrcesList ::= ("http://" + line.substring(2))
      println(s"[info] result: ${imgSrcesList}")
    }

    sendResultEmail("mjpark03@gmail.com", hrefsList, imgSrcesList)
  }


  def executeScript(area: String, count: Int) = {
    val cmd = Seq("casperjs", "test", Config.scriptSrc, Config.includeCmd,"--ignore-ssl-errors=true", s"--area=${area}", s"--count=${count}")
    cmd.!
    println("[info] script finished")
  }


  def sendResultEmail(email: String, hrefs: List[String], imgSrces: List[String]) = {
    try {
      val (body, title) =
        (MinbakAPIMailer.getEmailSearchResultBody(email, hrefs, imgSrces),
          s"[Minbak Crawling] Search Result for you")
      println("body: " + body)
      Mailer.send("info@korbit.co.kr", None, email, title, body)
      true
    } catch {
      case e: Throwable => println(s"Unexpected sending email error ${email} " + e.toString); false
    }
  }


  def findCodeForHanintel(area: String) = {

    val code = area.toLowerCase match {
      case "washington" => "10"
      case _ => "0"
    }

    code
  }

}