package controllers


import play.api._
import play.api.data.Form
import play.api.data.Forms._
import play.api.mvc._
import services.SearchService


case class SearchData(area: String, count: String)

object MainController extends Controller {

  val searchForm = Form(
    mapping(
      "area" -> nonEmptyText,
      "count" -> nonEmptyText
    )(SearchData.apply)(SearchData.unapply)
  )

  def viewMain = Action { implicit request =>
    Ok(views.html.mainView(searchForm))
  }

  def requestSearch = Action { implicit request =>
    searchForm.bindFromRequest.fold(
      error => Ok("error"),
      searchData => {
        println(s"area: ${searchData.area}, count: ${searchData.count}")

        SearchService.requestSearch(searchData.area, searchData.count.toInt)

        Ok(views.html.searchSuccess())
      }
    )
  }

}